from flufl.enum import Enum


class Fruit(Enum):
    kiwi = 1
    banana = 2
    tomato = 3
